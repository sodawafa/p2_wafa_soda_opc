# Projet Reservia

> Projet n ° 2 dans le cadre de ma formation de développeur Front-End avec Openclassrooms

## URL Projet

[reservia](https://sodawafa.gitlab.io/p2_wafa_soda_opc/)

## IDE

[PhpStorm](https://www.jetbrains.com/fr-fr/phpstorm/)

## Auteurs

[Wafa SODA](https://gitlab.com/sodawafa)




